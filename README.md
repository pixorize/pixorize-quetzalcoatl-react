# Pixorize Quetzalcoatl React
A [React](https://reactjs.org/) wrapper component for the
[@pixorize/pixorize-quetzalcoatl](https://www.npmjs.com/package/@pixorize/pixorize-quetzalcoatl) library.


## Instalation
```shell
npm install @pixorize/pixorize-quetzalcoatl-react
```
or
```shell
yarn add @pixorize/pixorize-quetzalcoatl-react
```


## Usage
```jsx
import React from 'react';
import PixorizeQuetzalcoatlReact from '@pixorize/pixorize-quetzalcoatl-react';

const data = '../path/to/data';
const imageUrl = 'https://via.placeholder.com/1280x720';

const InteractiveImage = () => {
  render(
    <div className="my-class-name">
      <PixorizeQuetzalcoatlReact
        data={data}
        imageUrl={imageUrl}
      />
    </<div>
  );
};

export default InteractiveImage;
```


## Documentation

### Props
`data`: `JSON`
The interactive image JSON data. See
[@pixorize/pixorize-quetzalcoatl](https://www.npmjs.com/package/@pixorize/pixorize-quetzalcoatl)
docs for more details.

`imageUrl`: `string`
The image URL.


---


## License
[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)

- [MIT license](https://bitbucket.org/pixorize/pixorize-quetzalcoatl-react/src/master/LICENSE.md)
- Copyright 2020 © [Pixorize Inc.](https://pixorize.com/)
