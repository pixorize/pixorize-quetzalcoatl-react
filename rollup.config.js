import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import babel from 'rollup-plugin-babel';
import { uglify } from 'rollup-plugin-uglify';

import banner from './banner';
import pkg from './package.json';

const config = {
  input: 'index.jsx',
  plugins: [
    babel({ exclude: 'node_modules/**' }),
    replace({'#__VERSION__#': pkg.version, delimiters: ['', '']}),
    resolve(),
    commonjs(),
  ],
  output: {
    banner: banner,
    format: 'cjs',
    name: 'PixorizeQuetzalcoatlReact',
    exports: 'default',
    sourcemap: true,
    globals: {
      react: 'React',
      'react-dom': 'ReactDOM',
      'prop-types': 'PropTypes',
      'PixorizeQuetzalcoatl': '@pixorize/pixorize-quetzalcoatl',
    },
  },
  external: ['react', 'react-dom', 'prop-types', '@pixorize/pixorize-quetzalcoatl'],
};

export default [
	{
		input: config.input,
		plugins: config.plugins,
		output: Object.assign({}, config.output, { file: './dist/index.js' }),
    external: config.external,
  },
  {
		input: config.input,
		plugins: config.plugins.concat(config.plugins, [
      uglify({
        sourcemap: true,
        output: {
          comments: function (node, comment) {
            const text = comment.value;
            const type = comment.type;

            // Multiline comment
            if (type === 'comment2') {
              return /Pixorize/.test(text);
            }

            return false;
          },
        },
      }),
    ]),
		output: Object.assign({}, config.output, { file: './dist/index.min.js' }),
    external: config.external,
	},
];
