var pkg = require('./package.json');
var date = new Date();

const banner = `/*! ${pkg.title || pkg.name} - v${pkg.version} - ${[ date.getFullYear(), ('0' + (date.getMonth() + 1)).slice(-2), ('0' + date.getDate()).slice(-2)].join('-')}
 * https://pixorize.com/
 *
 * Copyright (c) Pixorize Inc.
 * Licensed under the ${pkg.license} license */`;

export default banner;
