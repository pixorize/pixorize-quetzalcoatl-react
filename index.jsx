import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import PixorizeQuetzalcoatl from '@pixorize/pixorize-quetzalcoatl';

class PixorizeQuetzalcoatlReact extends React.Component {
  constructor(props) {
    super(props);

    this.interactiveImageLib = null;
    this.onWindowsResize = this.onWindowsResize.bind(this);
    this.setupInteractiveImage = this.setupInteractiveImage.bind(this);
    this.destroyAndCleanupInteractiveImage = this.destroyAndCleanupInteractiveImage.bind(this);
  }

  componentDidMount() {
    this.setupInteractiveImage();
  }

  componentDidUpdate() {
    const { imageUrl } = this.props;
    const imageHasChanged = (imageUrl !== this.oldImageUrl);
    if (!imageHasChanged) return;

    this.destroyAndCleanupInteractiveImage();
    this.setupInteractiveImage();
  }

  componentWillUnmount() {
    this.destroyAndCleanupInteractiveImage();
  }

  onWindowsResize() {
    const parentNode = ReactDOM.findDOMNode(this).parentElement;
    this.interactiveImageLib.onWindowsResize(parentNode.offsetWidth, parentNode.offsetHeight);
  }

  setupInteractiveImage() {
    const { data, imageUrl } = this.props;
    const parentNode = ReactDOM.findDOMNode(this).parentElement;

    this.interactiveImageLib = new PixorizeQuetzalcoatl(
      data,
      imageUrl,
      parentNode,
    );
    this.oldImageUrl = imageUrl;

    window.addEventListener('resize', this.onWindowsResize);
  }

  destroyAndCleanupInteractiveImage() {
    if (this.interactiveImageLib) {
      this.interactiveImageLib.destroy();
    }

    window.removeEventListener('resize', this.onWindowsResize);
  }

  render() {
    return (<div />);
  }
}

PixorizeQuetzalcoatlReact.propTypes = {
  data: PropTypes.shape({
    json: PropTypes.shape({
      scale: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).isRequired,
      dots: PropTypes.arrayOf(
        PropTypes.object
      ).isRequired,
    }).isRequired,
  }).isRequired,
  imageUrl: PropTypes.string.isRequired,
};

export default PixorizeQuetzalcoatlReact;
